package com.carinae.viewmodeltest

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class RotationalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rotational)

        if (savedInstanceState != null) {
            Log.v(LOG_TAG, "onCreate(savedInstanceState != null)")
        } else {
            Log.v(LOG_TAG, "onCreate(savedInstanceState == null)")
        }
    }

    override fun onDestroy() {
        Log.v(LOG_TAG, "onDestroy()")

        super.onDestroy()
    }

    companion object {
        val LOG_TAG = RotationalActivity::class.java.simpleName
    }
}
