package com.carinae.viewmodeltest

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.requireViewById
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

class MainActivity : AppCompatActivity() {

    private val buttonIncrementCount by bind<Button>(R.id.buttonIncrementCount)
    private val buttonStartActivity by bind<Button>(R.id.buttonStartActivity)
    private val textViewCountDisplay by bind<TextView>(R.id.textViewCountDisplay)
    private val mainViewModel: MainViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.count.observe(this, Observer<Int> { value ->
            if (value != null) {
                textViewCountDisplay.text = getString(R.string.increment_count_display, value)
            }
        })

        buttonIncrementCount.setOnClickListener { mainViewModel.incrementCountPressed() }
        buttonStartActivity.setOnClickListener { mainViewModel.startActivityPressed(this) }

        if (savedInstanceState != null) {
            Log.v(LOG_TAG, "onCreate(savedInstanceState != null)")
        } else {
            Log.v(LOG_TAG, "onCreate(savedInstanceState == null)")
        }
    }

    override fun onDestroy() {
        Log.v(LOG_TAG, "onDestroy()")

        super.onDestroy()
    }

    fun <T : View> Activity.bind(@IdRes res: Int): Lazy<T> {
        @Suppress("UNCHECKED_CAST")
        return lazy { requireViewById(this, res) as T }
    }

    companion object {
        val LOG_TAG = MainActivity::class.java.simpleName
    }
}
