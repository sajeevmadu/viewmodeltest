package com.carinae.viewmodeltest

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class MainViewModel(application: Application) : AndroidViewModel(application) {

    val count = MutableLiveData<Int>()

    init {
        Log.v(LOG_TAG, "new MainViewModel()")
        count.value = 0
    }

    fun startActivityPressed(mainActivity: MainActivity) {
        val intent = Intent(mainActivity, RotationalActivity::class.java)
        mainActivity.startActivity(intent)
        Log.v(LOG_TAG, "start RotationalActivity")
    }

    fun incrementCountPressed() {
        var countValue = count.value;
        if (countValue != null) {
            countValue = countValue.plus(1)
            count.value = countValue
            Log.v(LOG_TAG, "incrementCountPressed() $countValue")
        }
    }

    override fun onCleared() {
        Log.v(LOG_TAG, "onCleared()")

        super.onCleared()
    }

    companion object {
        val LOG_TAG = MainViewModel::class.java.simpleName
    }
}
