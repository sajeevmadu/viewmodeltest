package com.carinae.fixedviewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment

class ViewModelFragment : androidx.fragment.app.Fragment() {

    private val viewModelStore = ViewModelStore()
    private lateinit var factory: ViewModelProvider.Factory

    init {
        // Retain this fragment across configuration changes.
        retainInstance = true
    }

    fun setFactory(factory: ViewModelProvider.Factory) {
        this.factory = factory
    }

    /**
     * Returns an existing ViewModel or creates a new one in the scope (usually, a fragment or
     * an activity), associated with this `ViewModelProvider`.
     *
     *
     * The created ViewModel is associated with the given scope and will be retained
     * as long as the scope is alive (e.g. if it is an activity, until it is
     * finished or process is killed).
     *
     * @param modelClass The class of the ViewModel to create an instance of it if it is not
     * present.
     * @param <T>        The type parameter for the ViewModel.
     * @return A ViewModel that is an instance of the given type `T`.
    </T> */
    operator fun <T : ViewModel> get(modelClass: Class<T>): T {
        val canonicalName = modelClass.canonicalName
                ?: throw IllegalArgumentException("Local and anonymous classes can not be ViewModels")
        return get("$DEFAULT_KEY:$canonicalName", modelClass)
    }

    /**
     * Returns an existing ViewModel or creates a new one in the scope (usually, a fragment or
     * an activity), associated with this `ViewModelProvider`.
     *
     *
     * The created ViewModel is associated with the given scope and will be retained
     * as long as the scope is alive (e.g. if it is an activity, until it is
     * finished or process is killed).
     *
     * @param key        The key to use to identify the ViewModel.
     * @param modelClass The class of the ViewModel to create an instance of it if it is not
     * present.
     * @param <T>        The type parameter for the ViewModel.
     * @return A ViewModel that is an instance of the given type `T`.
    </T> */
    @MainThread
    operator fun <T : ViewModel> get(
            key: String,
            modelClass: Class<T>): T {
        var viewModel = viewModelStore.get(key)

        if (viewModel != null && modelClass.isInstance(viewModel)) {

            @Suppress("UNCHECKED_CAST")
            return viewModel as T
        }

        viewModel = factory.create(modelClass)
        viewModelStore.put(key, viewModel)

        return viewModel
    }

    override fun onDestroy() {
        viewModelStore.clear()

        super.onDestroy()
    }

    companion object {
        private val DEFAULT_KEY = "com.omnitracs.mvp.contract.DefaultKey"
    }
}
