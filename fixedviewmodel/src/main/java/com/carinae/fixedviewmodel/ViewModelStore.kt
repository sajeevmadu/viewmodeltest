package com.carinae.fixedviewmodel

import androidx.lifecycle.ViewModel
import java.lang.reflect.InvocationTargetException
import java.util.*

class ViewModelStore {
    private val mMap = HashMap<String, ViewModel>()

    internal fun put(
            key: String,
            viewModel: ViewModel) {
        val oldViewModel = mMap[key]
        if (oldViewModel != null) {
            invokeOnCleared(oldViewModel)
        }
        mMap[key] = viewModel
    }

    internal operator fun get(key: String): ViewModel? {
        val viewModel = mMap[key]
        return viewModel
    }

    /**
     * Clears internal storage and notifies ViewModels that they are no longer used.
     */
    fun clear() {
        for (viewModel in mMap.values) {
            invokeOnCleared(viewModel)
        }
        mMap.clear()
    }

    private fun invokeOnCleared(viewModel: ViewModel) {
        val viewModelClass = viewModel.javaClass
        try {
            val onClearedMethod = viewModelClass.getDeclaredMethod("onCleared")
            onClearedMethod.setAccessible(true)
            onClearedMethod.invoke(viewModel)
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }
}
