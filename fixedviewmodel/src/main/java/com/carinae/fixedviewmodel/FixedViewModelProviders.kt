package com.carinae.fixedviewmodel

import android.app.Activity
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.MainThread
import androidx.fragment.app.FragmentActivity

/**
 * Trying to resolve issue where another activity on top of current activity is rotated and returned
 * to original activity will cause the original activity's view model to be destroyed.
 */
object FixedViewModelProviders {
    private const val TAG_VIEW_MODEL_FRAGMENT = "FixedViewModelProviders.TAG_VIEW_MODEL_FRAGMENT"

    private fun checkApplication(activity: Activity): Application {
        val application = activity.application
                ?: throw IllegalStateException("Your activity/fragment is not yet attached to " + "Application. You can't request ViewModel before onCreate call.")
        return activity.application
                ?: throw IllegalStateException("Your activity/fragment is not yet attached to " + "Application. You can't request ViewModel before onCreate call.")
    }

    /**
     * Creates a [ViewModelProvider], which retains ViewModels while a scope of given Activity
     * is alive. More detailed explanation is in [ViewModel].
     *
     *
     * It uses [ViewModelProvider.AndroidViewModelFactory] to instantiate new ViewModels.
     *
     * @param activity an activity, in whose scope ViewModels should be retained
     * @return a ViewModelProvider instance
     */
    @MainThread
    fun of(activity: androidx.fragment.app.FragmentActivity): ViewModelFragment {
        val application = checkApplication(activity)

        val factory = ViewModelProvider.AndroidViewModelFactory.getInstance(
                application)

        return of(activity, factory)
    }

    /**
     * Creates a [ViewModelProvider], which retains ViewModels while a scope of given Activity
     * is alive. More detailed explanation is in [ViewModel].
     *
     *
     * It uses the given [ViewModelProvider.Factory] to instantiate new ViewModels.
     *
     * @param activity an activity, in whose scope ViewModels should be retained
     * @param factory  a `Factory` to instantiate new ViewModels
     * @return a ViewModelProvider instance
     */
    @MainThread
    fun of(
            activity: androidx.fragment.app.FragmentActivity,
            factory: ViewModelProvider.Factory): ViewModelFragment {
        val canonicalName = factory.javaClass.canonicalName
                ?: throw IllegalArgumentException("Local and anonymous classes can not be ViewModels")
        val fragmentTagName = "$TAG_VIEW_MODEL_FRAGMENT:$canonicalName"
        val fragmentManager = activity.supportFragmentManager
        var viewModelFragment: ViewModelFragment? = fragmentManager.findFragmentByTag(fragmentTagName) as ViewModelFragment?

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (viewModelFragment == null) {
            viewModelFragment = ViewModelFragment()
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(viewModelFragment, fragmentTagName)
            fragmentTransaction.commit()
        }

        viewModelFragment.setFactory(factory)

        return viewModelFragment
    }
}
